<div class="appeals view">
<h2><?php echo __('Appeal'); ?></h2>
	<dl>
		<dt><?php echo __('A P P E A L  I D'); ?></dt>
		<dd>
			<?php echo h($appeal['Appeal']['APPEAL_ID']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('A P P E A L  Description'); ?></dt>
		<dd>
			<?php echo h($appeal['Appeal']['APPEAL_Description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('A P P E A L  Start  Time  Date'); ?></dt>
		<dd>
			<?php echo h($appeal['Appeal']['APPEAL_Start_Time_Date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('A P P E A L  End  Time  Date'); ?></dt>
		<dd>
			<?php echo h($appeal['Appeal']['APPEAL_End_Time_Date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('A P P E A L  Type'); ?></dt>
		<dd>
			<?php echo h($appeal['Appeal']['APPEAL_Type']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Appeal'), array('action' => 'edit', $appeal['Appeal']['APPEAL_ID'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Appeal'), array('action' => 'delete', $appeal['Appeal']['APPEAL_ID']), array(), __('Are you sure you want to delete # %s?', $appeal['Appeal']['APPEAL_ID'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Appeals'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Appeal'), array('action' => 'add')); ?> </li>
	</ul>
</div>
