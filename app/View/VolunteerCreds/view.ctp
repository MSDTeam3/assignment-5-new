<div class="librarianCreds view">
<h2><?php echo __('Volunteer Cred'); ?></h2>
	<dl>
		<dt><?php echo __('Vol  Username'); ?></dt>
		<dd>
			<?php echo h($librarianCred['VolunteerCred']['Vol_Username']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Vol  Password'); ?></dt>
		<dd>
			<?php echo h($librarianCred['VolunteerCred']['Vol_Password']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Vol Sec Question'); ?></dt>
		<dd>
			<?php echo h($librarianCred['VolunteerCred']['Vol_secQuestion']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Vol Sec Answer'); ?></dt>
		<dd>
			<?php echo h($librarianCred['VolunteerCred']['Vol_secAnswer']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Volunteer  I D'); ?></dt>
		<dd>
			<?php echo h($librarianCred['VolunteerCred']['Volunteer_ID']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Volunteer Cred'), array('action' => 'edit', $librarianCred['VolunteerCred']['Vol_Username'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Volunteer Cred'), array('action' => 'delete', $librarianCred['VolunteerCred']['Vol_Username']), array(), __('Are you sure you want to delete # %s?', $librarianCred['VolunteerCred']['Vol_Username'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Volunteer Creds'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Volunteer Cred'), array('action' => 'add')); ?> </li>
	</ul>
</div>
