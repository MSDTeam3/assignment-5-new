<div class="volunteerCreds index">
	<h2><?php echo __('Volunteer Creds'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('Vol_Username'); ?></th>
			<th><?php echo $this->Paginator->sort('Vol_Password'); ?></th>
			<th><?php echo $this->Paginator->sort('Vol_secQuestion'); ?></th>
			<th><?php echo $this->Paginator->sort('Vol_secAnswer'); ?></th>
			<th><?php echo $this->Paginator->sort('Volunteer_ID'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($volunteerCreds as $volunteerCred): ?>
	<tr>
		<td><?php echo h($volunteerCred['VolunteerCred']['Vol_Username']); ?>&nbsp;</td>
		<td><?php echo h($volunteerCred['VolunteerCred']['Vol_Password']); ?>&nbsp;</td>
		<td><?php echo h($volunteerCred['VolunteerCred']['Vol_secQuestion']); ?>&nbsp;</td>
		<td><?php echo h($volunteerCred['VolunteerCred']['Vol_secAnswer']); ?>&nbsp;</td>
		<td><?php echo h($volunteerCred['VolunteerCred']['Volunteer_ID']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $volunteerCred['VolunteerCred']['Vol_Username'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $volunteerCred['VolunteerCred']['Vol_Username'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $volunteerCred['VolunteerCred']['Vol_Username']), array('confirm' => __('Are you sure you want to delete # %s?', $volunteerCred['VolunteerCred']['Vol_Username']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Volunteer Cred'), array('action' => 'add')); ?></li>
	</ul>
</div>
