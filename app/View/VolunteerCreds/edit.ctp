<div class="librarianCreds form">
<?php echo $this->Form->create('VolunteerCred'); ?>
	<fieldset>
		<legend><?php echo __('Edit Volunteer Cred'); ?></legend>
	<?php
		echo $this->Form->input('Vol_Username');
		echo $this->Form->input('Vol_Password');
		echo $this->Form->input('Vol_secQuestion');
		echo $this->Form->input('Vol_secAnswer');
		echo $this->Form->input('Volunteer_ID');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('VolunteerCred.Vol_Username')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('VolunteerCred.Vol_Username'))); ?></li>
		<li><?php echo $this->Html->link(__('List Volunteer Creds'), array('action' => 'index')); ?></li>
	</ul>
</div>
