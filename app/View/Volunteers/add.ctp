<div class="volunteers form">
<?php echo $this->Form->create('Volunteer'); ?>
	<fieldset>
		<legend><?php echo __('Add Volunteer'); ?></legend>
	<?php
		echo $this->Form->input('VOLUNTEER_fName');
		echo $this->Form->input('VOLUNTEER_lName');
		echo $this->Form->input('VOLUNTEER_Email');
		echo $this->Form->input('VOLUNTEER_ContactNo');
		echo $this->Form->input('LIBRARY_ID');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Volunteers'), array('action' => 'index')); ?></li>
	</ul>
</div>
