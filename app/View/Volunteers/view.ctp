<div class="volunteers view">
<h2><?php echo __('Volunteer'); ?></h2>
	<dl>
		<dt><?php echo __('V O L U N T E E R  I D'); ?></dt>
		<dd>
			<?php echo h($volunteer['Volunteer']['VOLUNTEER_ID']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('V O L U N T E E R F Name'); ?></dt>
		<dd>
			<?php echo h($volunteer['Volunteer']['VOLUNTEER_fName']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('V O L U N T E E R L Name'); ?></dt>
		<dd>
			<?php echo h($volunteer['Volunteer']['VOLUNTEER_lName']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('V O L U N T E E R  Email'); ?></dt>
		<dd>
			<?php echo h($volunteer['Volunteer']['VOLUNTEER_Email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('V O L U N T E E R  Contact No'); ?></dt>
		<dd>
			<?php echo h($volunteer['Volunteer']['VOLUNTEER_ContactNo']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('L I B R A R Y  I D'); ?></dt>
		<dd>
			<?php echo h($volunteer['Volunteer']['LIBRARY_ID']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Volunteer'), array('action' => 'edit', $volunteer['Volunteer']['VOLUNTEER_ID'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Volunteer'), array('action' => 'delete', $volunteer['Volunteer']['VOLUNTEER_ID']), array(), __('Are you sure you want to delete # %s?', $volunteer['Volunteer']['VOLUNTEER_ID'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Volunteers'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Volunteer'), array('action' => 'add')); ?> </li>
	</ul>
</div>
