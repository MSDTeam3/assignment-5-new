<div class="libraries view">
<h2><?php echo __('Library'); ?></h2>
	<dl>
		<dt><?php echo __('L I B R A R Y  I D'); ?></dt>
		<dd>
			<?php echo h($library['Library']['LIBRARY_ID']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('L I B R A R Y  Name'); ?></dt>
		<dd>
			<?php echo h($library['Library']['LIBRARY_Name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('L I B R A R Y  Address'); ?></dt>
		<dd>
			<?php echo h($library['Library']['LIBRARY_Address']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('L I B R A R Y  City'); ?></dt>
		<dd>
			<?php echo h($library['Library']['LIBRARY_City']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('L I B R A R Y  State'); ?></dt>
		<dd>
			<?php echo h($library['Library']['LIBRARY_State']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('L I B R A R Y  Zip'); ?></dt>
		<dd>
			<?php echo h($library['Library']['LIBRARY_Zip']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('L I B R A R Y  Work  Phone'); ?></dt>
		<dd>
			<?php echo h($library['Library']['LIBRARY_Work_Phone']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Library'), array('action' => 'edit', $library['Library']['LIBRARY_ID'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Library'), array('action' => 'delete', $library['Library']['LIBRARY_ID']), array(), __('Are you sure you want to delete # %s?', $library['Library']['LIBRARY_ID'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Libraries'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Library'), array('action' => 'add')); ?> </li>
	</ul>
</div>
