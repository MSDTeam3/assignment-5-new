<div class="libraries form">
<?php echo $this->Form->create('Library'); ?>
	<fieldset>
		<legend><?php echo __('Edit Library'); ?></legend>
	<?php
		echo $this->Form->input('LIBRARY_ID');
		echo $this->Form->input('LIBRARY_Name');
		echo $this->Form->input('LIBRARY_Address');
		echo $this->Form->input('LIBRARY_City');
		echo $this->Form->input('LIBRARY_State');
		echo $this->Form->input('LIBRARY_Zip');
		echo $this->Form->input('LIBRARY_Work_Phone');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Library.LIBRARY_ID')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Library.LIBRARY_ID'))); ?></li>
		<li><?php echo $this->Html->link(__('List Libraries'), array('action' => 'index')); ?></li>
	</ul>
</div>
