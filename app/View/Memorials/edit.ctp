<div class="memorials form">
<?php echo $this->Form->create('Memorial'); ?>
	<fieldset>
		<legend><?php echo __('Edit Memorial'); ?></legend>
	<?php
		echo $this->Form->input('Mem_ID');
		echo $this->Form->input('Mem_Party');
		echo $this->Form->input('Mem_Desciption');
		echo $this->Form->input('Mem_Inscription');
		echo $this->Form->input('Item_ID');
		echo $this->Form->input('DONOR_ID');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Memorial.Mem_ID')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Memorial.Mem_ID'))); ?></li>
		<li><?php echo $this->Html->link(__('List Memorials'), array('action' => 'index')); ?></li>
	</ul>
</div>
