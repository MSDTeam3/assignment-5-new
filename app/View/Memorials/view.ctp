<div class="memorials view">
<h2><?php echo __('Memorial'); ?></h2>
	<dl>
		<dt><?php echo __('Mem  I D'); ?></dt>
		<dd>
			<?php echo h($memorial['Memorial']['Mem_ID']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Mem  Party'); ?></dt>
		<dd>
			<?php echo h($memorial['Memorial']['Mem_Party']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Mem  Desciption'); ?></dt>
		<dd>
			<?php echo h($memorial['Memorial']['Mem_Desciption']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Mem  Inscription'); ?></dt>
		<dd>
			<?php echo h($memorial['Memorial']['Mem_Inscription']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Item  I D'); ?></dt>
		<dd>
			<?php echo h($memorial['Memorial']['Item_ID']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('D O N O R  I D'); ?></dt>
		<dd>
			<?php echo h($memorial['Memorial']['DONOR_ID']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Memorial'), array('action' => 'edit', $memorial['Memorial']['Mem_ID'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Memorial'), array('action' => 'delete', $memorial['Memorial']['Mem_ID']), array(), __('Are you sure you want to delete # %s?', $memorial['Memorial']['Mem_ID'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Memorials'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Memorial'), array('action' => 'add')); ?> </li>
	</ul>
</div>
