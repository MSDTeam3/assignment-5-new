<div class="donors index">
	<h2><?php echo __('Donors'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('DONOR_ID'); ?></th>
			<th><?php echo $this->Paginator->sort('DONOR_fName'); ?></th>
			<th><?php echo $this->Paginator->sort('DONOR_lName'); ?></th>
			<th><?php echo $this->Paginator->sort('DONOR_Organization'); ?></th>
			<th><?php echo $this->Paginator->sort('DONOR_Street_Address'); ?></th>
			<th><?php echo $this->Paginator->sort('DONOR_City'); ?></th>
			<th><?php echo $this->Paginator->sort('DONOR_State'); ?></th>
			<th><?php echo $this->Paginator->sort('DONOR_Zip'); ?></th>
			<th><?php echo $this->Paginator->sort('DONOR_Email'); ?></th>
			<th><?php echo $this->Paginator->sort('DONOR_Phone'); ?></th>
			<th><?php echo $this->Paginator->sort('DONOR_Notes'); ?></th>
			<th><?php echo $this->Paginator->sort('APPEAL_ID'); ?></th>
			<th><?php echo $this->Paginator->sort('Item_ID'); ?></th>
			<th><?php echo $this->Paginator->sort('Mem_ID'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($donors as $donor): ?>
	<tr>
		<td><?php echo h($donor['Donor']['DONOR_ID']); ?>&nbsp;</td>
		<td><?php echo h($donor['Donor']['DONOR_fName']); ?>&nbsp;</td>
		<td><?php echo h($donor['Donor']['DONOR_lName']); ?>&nbsp;</td>
		<td><?php echo h($donor['Donor']['DONOR_Organization']); ?>&nbsp;</td>
		<td><?php echo h($donor['Donor']['DONOR_Street_Address']); ?>&nbsp;</td>
		<td><?php echo h($donor['Donor']['DONOR_City']); ?>&nbsp;</td>
		<td><?php echo h($donor['Donor']['DONOR_State']); ?>&nbsp;</td>
		<td><?php echo h($donor['Donor']['DONOR_Zip']); ?>&nbsp;</td>
		<td><?php echo h($donor['Donor']['DONOR_Email']); ?>&nbsp;</td>
		<td><?php echo h($donor['Donor']['DONOR_Phone']); ?>&nbsp;</td>
		<td><?php echo h($donor['Donor']['DONOR_Notes']); ?>&nbsp;</td>
		<td><?php echo h($donor['Donor']['APPEAL_ID']); ?>&nbsp;</td>
		<td><?php echo h($donor['Donor']['Item_ID']); ?>&nbsp;</td>
		<td><?php echo h($donor['Donor']['Mem_ID']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $donor['Donor']['DONOR_ID'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $donor['Donor']['DONOR_ID'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $donor['Donor']['DONOR_ID']), array('confirm' => __('Are you sure you want to delete # %s?', $donor['Donor']['DONOR_ID']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Donor'), array('action' => 'add')); ?></li>
	</ul>
</div>
