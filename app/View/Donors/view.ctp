<div class="donors view">
<h2><?php echo __('Donor'); ?></h2>
	<dl>
		<dt><?php echo __('D O N O R  I D'); ?></dt>
		<dd>
			<?php echo h($donor['Donor']['DONOR_ID']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('D O N O R F Name'); ?></dt>
		<dd>
			<?php echo h($donor['Donor']['DONOR_fName']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('D O N O R L Name'); ?></dt>
		<dd>
			<?php echo h($donor['Donor']['DONOR_lName']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('D O N O R  Organization'); ?></dt>
		<dd>
			<?php echo h($donor['Donor']['DONOR_Organization']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('D O N O R  Street  Address'); ?></dt>
		<dd>
			<?php echo h($donor['Donor']['DONOR_Street_Address']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('D O N O R  City'); ?></dt>
		<dd>
			<?php echo h($donor['Donor']['DONOR_City']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('D O N O R  State'); ?></dt>
		<dd>
			<?php echo h($donor['Donor']['DONOR_State']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('D O N O R  Zip'); ?></dt>
		<dd>
			<?php echo h($donor['Donor']['DONOR_Zip']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('D O N O R  Email'); ?></dt>
		<dd>
			<?php echo h($donor['Donor']['DONOR_Email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('D O N O R  Phone'); ?></dt>
		<dd>
			<?php echo h($donor['Donor']['DONOR_Phone']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('D O N O R  Notes'); ?></dt>
		<dd>
			<?php echo h($donor['Donor']['DONOR_Notes']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('A P P E A L  I D'); ?></dt>
		<dd>
			<?php echo h($donor['Donor']['APPEAL_ID']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Item  I D'); ?></dt>
		<dd>
			<?php echo h($donor['Donor']['Item_ID']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Mem  I D'); ?></dt>
		<dd>
			<?php echo h($donor['Donor']['Mem_ID']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Donor'), array('action' => 'edit', $donor['Donor']['y'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Donor'), array('action' => 'delete', $donor['Donor']['y']), array(), __('Are you sure you want to delete # %s?', $donor['Donor']['y'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Donors'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Donor'), array('action' => 'add')); ?> </li>
	</ul>
</div>
