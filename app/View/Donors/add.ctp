<div class="donors form">
<?php echo $this->Form->create('Donor'); ?>
	<fieldset>
		<legend><?php echo __('Add Donor'); ?></legend>
	<?php
		echo $this->Form->input('DONOR_ID');
		echo $this->Form->input('DONOR_fName');
		echo $this->Form->input('DONOR_lName');
		echo $this->Form->input('DONOR_Organization');
		echo $this->Form->input('DONOR_Street_Address');
		echo $this->Form->input('DONOR_City');
		echo $this->Form->input('DONOR_State');
		echo $this->Form->input('DONOR_Zip');
		echo $this->Form->input('DONOR_Email');
		echo $this->Form->input('DONOR_Phone');
		echo $this->Form->input('DONOR_Notes');
		echo $this->Form->input('APPEAL_ID');
		echo $this->Form->input('Item_ID');
		echo $this->Form->input('Mem_ID');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Donors'), array('action' => 'index')); ?></li>
	</ul>
</div>
