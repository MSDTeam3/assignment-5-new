<div class="librarianCreds view">
<h2><?php echo __('Librarian Cred'); ?></h2>
	<dl>
		<dt><?php echo __('Lib  Username'); ?></dt>
		<dd>
			<?php echo h($librarianCred['LibrarianCred']['Lib_Username']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Lib  Password'); ?></dt>
		<dd>
			<?php echo h($librarianCred['LibrarianCred']['Lib_Password']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Lib Sec Question'); ?></dt>
		<dd>
			<?php echo h($librarianCred['LibrarianCred']['Lib_secQuestion']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Lib Sec Answer'); ?></dt>
		<dd>
			<?php echo h($librarianCred['LibrarianCred']['Lib_secAnswer']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Librarian  I D'); ?></dt>
		<dd>
			<?php echo h($librarianCred['LibrarianCred']['Librarian_ID']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Librarian Cred'), array('action' => 'edit', $librarianCred['LibrarianCred']['Lib_Username'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Librarian Cred'), array('action' => 'delete', $librarianCred['LibrarianCred']['Lib_Username']), array(), __('Are you sure you want to delete # %s?', $librarianCred['LibrarianCred']['Lib_Username'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Librarian Creds'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Librarian Cred'), array('action' => 'add')); ?> </li>
	</ul>
</div>
