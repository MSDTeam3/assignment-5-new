<div class="librarianCreds form">
<?php echo $this->Form->create('LibrarianCred'); ?>
	<fieldset>
		<legend><?php echo __('Add Librarian Cred'); ?></legend>
	<?php
		echo $this->Form->input('Lib_Password');
		echo $this->Form->input('Lib_secQuestion');
		echo $this->Form->input('Lib_secAnswer');
		echo $this->Form->input('Librarian_ID');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Librarian Creds'), array('action' => 'index')); ?></li>
	</ul>
</div>
