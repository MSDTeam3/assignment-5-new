<div class="librarians view">
<h2><?php echo __('Librarian'); ?></h2>
	<dl>
		<dt><?php echo __('L I B R A R I A N  I D'); ?></dt>
		<dd>
			<?php echo h($librarian['Librarian']['LIBRARIAN_ID']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('L I B R A R I A N F Name'); ?></dt>
		<dd>
			<?php echo h($librarian['Librarian']['LIBRARIAN_fName']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('L I B R A R I A N L Name'); ?></dt>
		<dd>
			<?php echo h($librarian['Librarian']['LIBRARIAN_lName']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('L I B R A R I A N  Email'); ?></dt>
		<dd>
			<?php echo h($librarian['Librarian']['LIBRARIAN_Email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('L I B R A R I A N  Contact No'); ?></dt>
		<dd>
			<?php echo h($librarian['Librarian']['LIBRARIAN_ContactNo']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('L I B R A R Y  I D'); ?></dt>
		<dd>
			<?php echo h($librarian['Librarian']['LIBRARY_ID']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Librarian'), array('action' => 'edit', $librarian['Librarian']['LIBRARIAN_ID'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Librarian'), array('action' => 'delete', $librarian['Librarian']['LIBRARIAN_ID']), array(), __('Are you sure you want to delete # %s?', $librarian['Librarian']['LIBRARIAN_ID'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Librarians'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Librarian'), array('action' => 'add')); ?> </li>
	</ul>
</div>
