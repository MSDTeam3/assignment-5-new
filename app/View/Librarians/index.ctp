<div class="librarians index">
	<h2><?php echo __('Librarians'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('LIBRARIAN_ID'); ?></th>
			<th><?php echo $this->Paginator->sort('LIBRARIAN_fName'); ?></th>
			<th><?php echo $this->Paginator->sort('LIBRARIAN_lName'); ?></th>
			<th><?php echo $this->Paginator->sort('LIBRARIAN_Email'); ?></th>
			<th><?php echo $this->Paginator->sort('LIBRARIAN_ContactNo'); ?></th>
			<th><?php echo $this->Paginator->sort('LIBRARY_ID'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($librarians as $librarian): ?>
	<tr>
		<td><?php echo h($librarian['Librarian']['LIBRARIAN_ID']); ?>&nbsp;</td>
		<td><?php echo h($librarian['Librarian']['LIBRARIAN_fName']); ?>&nbsp;</td>
		<td><?php echo h($librarian['Librarian']['LIBRARIAN_lName']); ?>&nbsp;</td>
		<td><?php echo h($librarian['Librarian']['LIBRARIAN_Email']); ?>&nbsp;</td>
		<td><?php echo h($librarian['Librarian']['LIBRARIAN_ContactNo']); ?>&nbsp;</td>
		<td><?php echo h($librarian['Librarian']['LIBRARY_ID']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $librarian['Librarian']['LIBRARIAN_ID'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $librarian['Librarian']['LIBRARIAN_ID'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $librarian['Librarian']['LIBRARIAN_ID']), array('confirm' => __('Are you sure you want to delete # %s?', $librarian['Librarian']['LIBRARIAN_ID']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Librarian'), array('action' => 'add')); ?></li>
	</ul>
</div>
