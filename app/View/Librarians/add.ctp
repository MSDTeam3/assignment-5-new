<div class="librarians form">
<?php echo $this->Form->create('Librarian'); ?>
	<fieldset>
		<legend><?php echo __('Add Librarian'); ?></legend>
	<?php
		echo $this->Form->input('LIBRARIAN_fName');
		echo $this->Form->input('LIBRARIAN_lName');
		echo $this->Form->input('LIBRARIAN_Email');
		echo $this->Form->input('LIBRARIAN_ContactNo');
		echo $this->Form->input('LIBRARY_ID');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Librarians'), array('action' => 'index')); ?></li>
	</ul>
</div>
