<div class="superAdminCreds view">
<h2><?php echo __('Super Admin Cred'); ?></h2>
	<dl>
		<dt><?php echo __('S A  Username'); ?></dt>
		<dd>
			<?php echo h($superAdminCred['SuperAdminCred']['SA_Username']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('S A  Password'); ?></dt>
		<dd>
			<?php echo h($superAdminCred['SuperAdminCred']['SA_Password']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('S A Sec Question'); ?></dt>
		<dd>
			<?php echo h($superAdminCred['SuperAdminCred']['SA_secQuestion']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('S A Sec Answer'); ?></dt>
		<dd>
			<?php echo h($superAdminCred['SuperAdminCred']['SA_secAnswer']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Library  I D'); ?></dt>
		<dd>
			<?php echo h($superAdminCred['SuperAdminCred']['Library_ID']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Super Admin Cred'), array('action' => 'edit', $superAdminCred['SuperAdminCred']['y'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Super Admin Cred'), array('action' => 'delete', $superAdminCred['SuperAdminCred']['y']), array(), __('Are you sure you want to delete # %s?', $superAdminCred['SuperAdminCred']['y'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Super Admin Creds'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Super Admin Cred'), array('action' => 'add')); ?> </li>
	</ul>
</div>
