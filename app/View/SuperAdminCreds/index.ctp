<div class="superAdminCreds index">
	<h2><?php echo __('Super Admin Creds'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('SA_Username'); ?></th>
			<th><?php echo $this->Paginator->sort('SA_Password'); ?></th>
			<th><?php echo $this->Paginator->sort('SA_secQuestion'); ?></th>
			<th><?php echo $this->Paginator->sort('SA_secAnswer'); ?></th>
			<th><?php echo $this->Paginator->sort('Library_ID'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($superAdminCreds as $superAdminCred): ?>
	<tr>
		<td><?php echo h($superAdminCred['SuperAdminCred']['SA_Username']); ?>&nbsp;</td>
		<td><?php echo h($superAdminCred['SuperAdminCred']['SA_Password']); ?>&nbsp;</td>
		<td><?php echo h($superAdminCred['SuperAdminCred']['SA_secQuestion']); ?>&nbsp;</td>
		<td><?php echo h($superAdminCred['SuperAdminCred']['SA_secAnswer']); ?>&nbsp;</td>
		<td><?php echo h($superAdminCred['SuperAdminCred']['Library_ID']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $superAdminCred['SuperAdminCred']['y'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $superAdminCred['SuperAdminCred']['y'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $superAdminCred['SuperAdminCred']['y']), array('confirm' => __('Are you sure you want to delete # %s?', $superAdminCred['SuperAdminCred']['y']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Super Admin Cred'), array('action' => 'add')); ?></li>
	</ul>
</div>
