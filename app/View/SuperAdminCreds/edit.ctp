<div class="superAdminCreds form">
<?php echo $this->Form->create('SuperAdminCred'); ?>
	<fieldset>
		<legend><?php echo __('Edit Super Admin Cred'); ?></legend>
	<?php
		echo $this->Form->input('SA_Username');
		echo $this->Form->input('SA_Password');
		echo $this->Form->input('SA_secQuestion');
		echo $this->Form->input('SA_secAnswer');
		echo $this->Form->input('Library_ID');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('SuperAdminCred.y')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('SuperAdminCred.y'))); ?></li>
		<li><?php echo $this->Html->link(__('List Super Admin Creds'), array('action' => 'index')); ?></li>
	</ul>
</div>
