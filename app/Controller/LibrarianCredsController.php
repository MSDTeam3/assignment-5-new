<?php
App::uses('AppController', 'Controller');
/**
 * LibrarianCreds Controller
 *
 * @property LibrarianCred $LibrarianCred
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class LibrarianCredsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->LibrarianCred->recursive = 0;
		$this->set('LibrarianCreds', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->LibrarianCred->exists($id)) {
			throw new NotFoundException(__('Invalid Librarian cred'));
		}
		$options = array('conditions' => array('LibrarianCred.' . $this->LibrarianCred->primaryKey => $id));
		$this->set('LibrarianCred', $this->LibrarianCred->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->LibrarianCred->create();
			if ($this->LibrarianCred->save($this->request->data)) {
				$this->Session->setFlash(__('The Librarian cred has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The Librarian cred could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->LibrarianCred->exists($id)) {
			throw new NotFoundException(__('Invalid Librarian cred'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->LibrarianCred->save($this->request->data)) {
				$this->Session->setFlash(__('The Librarian cred has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The Librarian cred could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('LibrarianCred.' . $this->LibrarianCred->primaryKey => $id));
			$this->request->data = $this->LibrarianCred->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->LibrarianCred->id = $id;
		if (!$this->LibrarianCred->exists()) {
			throw new NotFoundException(__('Invalid Librarian cred'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->LibrarianCred->delete()) {
			$this->Session->setFlash(__('The Librarian cred has been deleted.'));
		} else {
			$this->Session->setFlash(__('The Librarian cred could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
