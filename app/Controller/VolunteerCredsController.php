<?php
App::uses('AppController', 'Controller');
/**
 * VolunteerCreds Controller
 *
 * @property VolunteerCred $VolunteerCred
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class VolunteerCredsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->VolunteerCred->recursive = 0;
		$this->set('volunteerCreds', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->VolunteerCred->exists($id)) {
			throw new NotFoundException(__('Invalid volunteer cred'));
		}
		$options = array('conditions' => array('VolunteerCred.' . $this->VolunteerCred->primaryKey => $id));
		$this->set('volunteerCred', $this->VolunteerCred->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->VolunteerCred->create();
			if ($this->VolunteerCred->save($this->request->data)) {
				$this->Session->setFlash(__('The Volunteer cred has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The Volunteer cred could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->VolunteerCred->exists($id)) {
			throw new NotFoundException(__('Invalid Volunteer cred'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->VolunteerCred->save($this->request->data)) {
				$this->Session->setFlash(__('The Volunteer cred has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The Volunteer cred could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('VolunteerCred.' . $this->VolunteerCred->primaryKey => $id));
			$this->request->data = $this->VolunteerCred->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->VolunteerCred->id = $id;
		if (!$this->VolunteerCred->exists()) {
			throw new NotFoundException(__('Invalid Volunteer cred'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->VolunteerCred->delete()) {
			$this->Session->setFlash(__('The Volunteer cred has been deleted.'));
		} else {
			$this->Session->setFlash(__('The Volunteer cred could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
