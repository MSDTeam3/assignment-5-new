<?php
App::uses('AppController', 'Controller');
/**
 * SuperAdminCreds Controller
 *
 * @property SuperAdminCred $SuperAdminCred
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class SuperAdminCredsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->SuperAdminCred->recursive = 0;
		$this->set('superAdminCreds', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->SuperAdminCred->exists($id)) {
			throw new NotFoundException(__('Invalid super admin cred'));
		}
		$options = array('conditions' => array('SuperAdminCred.' . $this->SuperAdminCred->primaryKey => $id));
		$this->set('superAdminCred', $this->SuperAdminCred->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->SuperAdminCred->create();
			if ($this->SuperAdminCred->save($this->request->data)) {
				$this->Session->setFlash(__('The super admin cred has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The super admin cred could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->SuperAdminCred->exists($id)) {
			throw new NotFoundException(__('Invalid super admin cred'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->SuperAdminCred->save($this->request->data)) {
				$this->Session->setFlash(__('The super admin cred has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The super admin cred could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('SuperAdminCred.' . $this->SuperAdminCred->primaryKey => $id));
			$this->request->data = $this->SuperAdminCred->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->SuperAdminCred->id = $id;
		if (!$this->SuperAdminCred->exists()) {
			throw new NotFoundException(__('Invalid super admin cred'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->SuperAdminCred->delete()) {
			$this->Session->setFlash(__('The super admin cred has been deleted.'));
		} else {
			$this->Session->setFlash(__('The super admin cred could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
