<?php
/**
 * VolunteerCredFixture
 *
 */
class VolunteerCredFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'Vol_Username' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 6, 'key' => 'primary', 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'Vol_Password' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'Vol_secQuestion' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'Vol_secAnswer' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'Volunteer_ID' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'Vol_Username', 'unique' => 1),
			'R_14' => array('column' => 'Volunteer_ID', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'Vol_Username' => 'Lore',
			'Vol_Password' => 'Lorem ipsum dolor sit amet',
			'Vol_secQuestion' => 'Lorem ipsum dolor sit amet',
			'Vol_secAnswer' => 'Lorem ipsum dolor sit amet',
			'Volunteer_ID' => 1
		),
	);

}
