<?php
/**
 * VolunteerFixture
 *
 */
class VolunteerFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'VOLUNTEER_ID' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'VOLUNTEER_fName' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'VOLUNTEER_lName' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'VOLUNTEER_Email' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'VOLUNTEER_ContactNo' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'length' => 10, 'unsigned' => false),
		'LIBRARY_ID' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'VOLUNTEER_ID', 'unique' => 1),
			'LIBRARY_ID' => array('column' => 'LIBRARY_ID', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'VOLUNTEER_ID' => 1,
			'VOLUNTEER_fName' => 'Lorem ipsum dolor sit amet',
			'VOLUNTEER_lName' => 'Lorem ipsum dolor sit amet',
			'VOLUNTEER_Email' => 'Lorem ipsum dolor sit amet',
			'VOLUNTEER_ContactNo' => '',
			'LIBRARY_ID' => 1
		),
	);

}
