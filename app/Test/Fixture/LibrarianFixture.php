<?php
/**
 * LibrarianFixture
 *
 */
class LibrarianFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'LIBRARIAN_ID' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'LIBRARIAN_fName' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'LIBRARIAN_lName' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'LIBRARIAN_Email' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'LIBRARIAN_ContactNo' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'length' => 10, 'unsigned' => false),
		'LIBRARY_ID' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'LIBRARIAN_ID', 'unique' => 1),
			'LIBRARY_ID' => array('column' => 'LIBRARY_ID', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'LIBRARIAN_ID' => 1,
			'LIBRARIAN_fName' => 'Lorem ipsum dolor sit amet',
			'LIBRARIAN_lName' => 'Lorem ipsum dolor sit amet',
			'LIBRARIAN_Email' => 'Lorem ipsum dolor sit amet',
			'LIBRARIAN_ContactNo' => '',
			'LIBRARY_ID' => 1
		),
	);

}
