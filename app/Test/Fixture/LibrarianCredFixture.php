<?php
/**
 * LibrarianCredFixture
 *
 */
class LibrarianCredFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'Lib_Username' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 6, 'key' => 'primary', 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'Lib_Password' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'Lib_secQuestion' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'Lib_secAnswer' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'Librarian_ID' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'Lib_Username', 'unique' => 1),
			'Librarian_ID' => array('column' => 'Librarian_ID', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'Lib_Username' => 'Lore',
			'Lib_Password' => 'Lorem ipsum dolor sit amet',
			'Lib_secQuestion' => 'Lorem ipsum dolor sit amet',
			'Lib_secAnswer' => 'Lorem ipsum dolor sit amet',
			'Librarian_ID' => 1
		),
	);

}
