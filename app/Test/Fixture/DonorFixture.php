<?php
/**
 * DonorFixture
 *
 */
class DonorFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'DONOR_ID' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'DONOR_fName' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'DONOR_lName' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'DONOR_Organization' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'DONOR_Street_Address' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'DONOR_City' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 18, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'DONOR_State' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 2, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'DONOR_Zip' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 5, 'unsigned' => false),
		'DONOR_Email' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'DONOR_Phone' => array('type' => 'biginteger', 'null' => true, 'default' => null, 'length' => 10, 'unsigned' => false),
		'DONOR_Notes' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'APPEAL_ID' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'Item_ID' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'Mem_ID' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'DONOR_ID', 'unique' => 1),
			'APPEAL_ID' => array('column' => 'APPEAL_ID', 'unique' => 0),
			'Item_ID' => array('column' => 'Item_ID', 'unique' => 0),
			'Mem_ID' => array('column' => 'Mem_ID', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'DONOR_ID' => 1,
			'DONOR_fName' => 'Lorem ipsum dolor sit amet',
			'DONOR_lName' => 'Lorem ipsum dolor sit amet',
			'DONOR_Organization' => 'Lorem ipsum dolor sit amet',
			'DONOR_Street_Address' => 'Lorem ipsum dolor sit amet',
			'DONOR_City' => 'Lorem ipsum dolo',
			'DONOR_State' => '',
			'DONOR_Zip' => 1,
			'DONOR_Email' => 'Lorem ipsum dolor sit amet',
			'DONOR_Phone' => '',
			'DONOR_Notes' => 'Lorem ipsum dolor sit amet',
			'APPEAL_ID' => 1,
			'Item_ID' => 1,
			'Mem_ID' => 1
		),
	);

}
