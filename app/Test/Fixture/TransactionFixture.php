<?php
/**
 * TransactionFixture
 *
 */
class TransactionFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'Trans_ID' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'Trans_Amount' => array('type' => 'float', 'null' => true, 'default' => null, 'length' => '10,2', 'unsigned' => false),
		'Trans_Time_Date' => array('type' => 'timestamp', 'null' => false, 'default' => null),
		'Trans_Type' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'Trans_Token' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'Item_ID' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'Library_ID' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'Donor_ID' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'Appeal_ID' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'Trans_ID', 'unique' => 1),
			'Item_ID' => array('column' => 'Item_ID', 'unique' => 0),
			'Library_ID' => array('column' => 'Library_ID', 'unique' => 0),
			'Donor_ID' => array('column' => 'Donor_ID', 'unique' => 0),
			'Appeal_ID' => array('column' => 'Appeal_ID', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'Trans_ID' => 1,
			'Trans_Amount' => 1,
			'Trans_Time_Date' => 1459806927,
			'Trans_Type' => 'Lorem ipsum dolor sit amet',
			'Trans_Token' => 'Lorem ipsum dolor sit amet',
			'Item_ID' => 1,
			'Library_ID' => 1,
			'Donor_ID' => 1,
			'Appeal_ID' => 1
		),
	);

}
