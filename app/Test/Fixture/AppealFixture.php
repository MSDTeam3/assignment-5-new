<?php
/**
 * AppealFixture
 *
 */
class AppealFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'APPEAL_ID' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'APPEAL_Description' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'APPEAL_Start_Time_Date' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'APPEAL_End_Time_Date' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'APPEAL_Type' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'APPEAL_ID', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'APPEAL_ID' => 1,
			'APPEAL_Description' => 'Lorem ipsum dolor sit amet',
			'APPEAL_Start_Time_Date' => '2016-04-04 23:14:05',
			'APPEAL_End_Time_Date' => '2016-04-04 23:14:05',
			'APPEAL_Type' => 'Lorem ipsum dolor sit amet'
		),
	);

}
