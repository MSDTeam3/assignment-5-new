<?php
/**
 * LibraryFixture
 *
 */
class LibraryFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'LIBRARY_ID' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'LIBRARY_Name' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'LIBRARY_Address' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'LIBRARY_City' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 18, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'LIBRARY_State' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 2, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'LIBRARY_Zip' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 5, 'unsigned' => false),
		'LIBRARY_Work_Phone' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => false),
		'indexes' => array(
			'PRIMARY' => array('column' => 'LIBRARY_ID', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'LIBRARY_ID' => 1,
			'LIBRARY_Name' => 'Lorem ipsum dolor sit amet',
			'LIBRARY_Address' => 'Lorem ipsum dolor sit amet',
			'LIBRARY_City' => 'Lorem ipsum dolo',
			'LIBRARY_State' => '',
			'LIBRARY_Zip' => 1,
			'LIBRARY_Work_Phone' => ''
		),
	);

}
