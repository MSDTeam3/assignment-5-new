<?php
App::uses('SuperAdminCred', 'Model');

/**
 * SuperAdminCred Test Case
 *
 */
class SuperAdminCredTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.super_admin_cred'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->SuperAdminCred = ClassRegistry::init('SuperAdminCred');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->SuperAdminCred);

		parent::tearDown();
	}

}
