<?php
App::uses('VolunteerCred', 'Model');

/**
 * VolunteerCred Test Case
 *
 */
class VolunteerCredTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.volunteer_cred'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->VolunteerCred = ClassRegistry::init('VolunteerCred');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->VolunteerCred);

		parent::tearDown();
	}

}
