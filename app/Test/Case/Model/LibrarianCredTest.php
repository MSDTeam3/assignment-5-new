<?php
App::uses('LibrarianCred', 'Model');

/**
 * LibrarianCred Test Case
 *
 */
class LibrarianCredTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.librarian_cred'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->LibrarianCred = ClassRegistry::init('LibrarianCred');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->LibrarianCred);

		parent::tearDown();
	}

}
